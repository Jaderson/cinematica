
export class SolicitacaoDTO {
    id:Number;
    funcionario:String;
    especialidade:String;
    horario:String;
    data:String;
}