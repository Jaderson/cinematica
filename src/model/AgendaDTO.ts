import { FuncionarioDTO } from "./FuncionarioDTO";
import { EspecialidadeDTO } from "./EspecialidadeDTO";
import { HorarioDTO } from "./HorarioDTO";
import { PerfilDTO } from "./PerfilDTO";

export class AgendaDTO {
    id:Number;
    funcionario:String;
    especialidade:String;
    horario:String;
    data:String;
}