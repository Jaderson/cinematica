import { FuncionarioDTO } from "./FuncionarioDTO";
import { EspecialidadeDTO } from "./EspecialidadeDTO";
import { HorarioDTO } from "./HorarioDTO";
import { PerfilDTO } from "./PerfilDTO";

export class AgendamentoDTO {
    funcionario:FuncionarioDTO;
    especialidade:EspecialidadeDTO;
    horario:HorarioDTO;
    perfil:PerfilDTO;
    dateSelecionada:string;
}