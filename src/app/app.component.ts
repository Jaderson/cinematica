import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HttpService } from '../providers/HttpService';
import { PerfilService } from '../providers/PerfilService';

import { HomePage } from '../pages/home/home';
import { Perfil } from '../pages/perfil/perfil';
import { Agendamento } from '../pages/agendamento/agendamento';
import { PerfilDTO } from '../model/PerfilDTO';
import { Login } from "../pages/login/login";
import { SolicitacaoPage } from "../pages/solicitacao/solicitacao";

@Component({
  templateUrl: 'app.html',
  providers:[HttpService]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = Login;
  pages: Array<{title: string, component: any, icon: string}>;
  icons: string[];
 
  perfilDTO = new PerfilDTO();

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
  public httpService:HttpService, public perfilService:PerfilService) {
    this.initializeApp();
    this.exibirPerfil();

     this.icons = ['md-home','md-person','md-calendar','md-clipboard','md-log-out','md-list']

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Inicio', component: HomePage, icon: this.icons[0]},
      { title: 'Conta', component: Perfil, icon: this.icons[1]},
      { title: 'Solicitar Consulta', component: Agendamento, icon: this.icons[2]},
      { title: 'Consultas Pendentes', component: SolicitacaoPage, icon: this.icons[5]},
      { title: 'Evolução', component: '', icon: this.icons[3]},
      { title: 'Sair', component: Login, icon: this.icons[4]},
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

   public exibirPerfil(){
     this.perfilService.visualizarPerfil(1,'jaderson').subscribe(
        data => {
          this.perfilDTO.nome = data.results[0].nome;
          this.perfilDTO.email = data.results[0].email;
          this.perfilDTO.dataNascimento = data.results[0].dataNascimento;
          this.perfilDTO.telefone = data.results[0].telefoneCelular;
        },
        err => {
          console.log(err);
        },
        () => {}
      );
    }
}
