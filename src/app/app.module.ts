import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, IonicPageModule, Refresher } from 'ionic-angular';
import { TranslateModule, TranslateStaticLoader, TranslateLoader} from 'ng2-translate/ng2-translate';
import { Http } from '@angular/http';

import { HttpService } from '../providers/HttpService';
import { PerfilService } from '../providers/PerfilService';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Perfil } from '../pages/perfil/perfil';
import { Agendamento } from '../pages/agendamento/agendamento';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FuncionarioService } from "../providers/FuncionarioService";
import { Datas } from "../pages/datas/datas";
import { HorarioService } from "../providers/HorarioService";
import { TextMaskModule } from 'angular2-text-mask';
import { Horario } from "../pages/horario/horario";
import { Especialidade } from "../pages/especialidade/especialidade";
import { EspecialidadeService } from "../providers/EspecialidadeService";
import { PerfilDTO } from "../model/PerfilDTO";
import { Login } from "../pages/login/login";
import { LoginService } from "../providers/LoginService";
import { AgendaService } from "../providers/AgendaService";
import { LoadingUtil } from "../util/LoadingUtil";
import { SolicitacaoService } from "../providers/SolicitacaoService";
import { SolicitacaoPage } from "../pages/solicitacao/solicitacao";
import { CalendarModule } from 'ion2-calendar';
import { DatePicker } from 'ionic2-date-picker';
import { DatePickerModule } from 'datepicker-ionic2';

export function createTranslateLoader(http: Http) {
    return new TranslateStaticLoader(http, 'assets/i18n', '.json');
}


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Perfil,
    Agendamento,
    Datas,
    Horario,
    Especialidade,
    Login,
    SolicitacaoPage,
    DatePicker
  ],
  imports: [
    BrowserModule,
    TextMaskModule,
    IonicModule.forRoot(MyApp),
    CalendarModule,
    DatePickerModule,
    TranslateModule.forRoot({
    provide: TranslateLoader,
    useFactory: (createTranslateLoader),
    deps: [Http]
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Perfil,
    Agendamento,
    Datas,
    Horario,
    Especialidade,
    Login,
    SolicitacaoPage,
    DatePicker
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpService,
    PerfilService,
    FuncionarioService,
    HorarioService,
    EspecialidadeService,
    AgendaService,
    LoadingUtil, 
    SolicitacaoService, 
    LoginService
  ]
})
export class AppModule {}    

