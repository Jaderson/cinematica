import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

@Injectable()
export class LoadingUtil {

    loader : any;
    SINCRONIZANDO : string = "Aguarde...";

    constructor(public translateService: TranslateService, public loadingCtrl:LoadingController) {

   }

   abrirLoading(){
     this.loader = this.loadingCtrl.create({
       content: this.traduzirCampo(this.SINCRONIZANDO)
     });
     this.loader.present();
   }

   fecharLoading(){
     this.loader.dismiss();
   }

   traduzirCampo(texto : string){
     this.translateService.get(texto).subscribe(
       value => {
         texto = value;
       }
     )
     return texto;
   }
}
