import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { HttpService } from './HttpService';

/*
  Generated class for the Perfil provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class PerfilService {    

  url:String = 'http://www.cinematicaesportiva.com.br/fisio-web/api/';
  urlLocal: string = 'http://localhost:8080/fisio-web/api/';

  constructor(public httpService: HttpService) {}

  public visualizarPerfil(idEmpresa:Number, nome:String){
    let url = this.url + 'pessoas/listar/' + idEmpresa + '/' + nome;  
    let response;
    response = this.httpService.get(url)
                  .map(res => res.json())
                  .catch(this.handleError);
      return response;
 
  }

   handleError(error: Response) {
        console.log(error);
        return Observable.throw(error.json() || 'Server error');
    }
}
