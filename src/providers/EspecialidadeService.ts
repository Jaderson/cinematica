import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from './HttpService';
import { LoadingUtil } from '../util/LoadingUtil';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

/*
  Generated class for the Especialidade provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class EspecialidadeService {

  url:String = 'http://www.cinematicaesportiva.com.br/fisio-web/api/';
  urlLocal: string = 'http://localhost:8080/fisio-web/api/';

  constructor(public httpService: HttpService, public loadingUtil:LoadingUtil) {}

  public getListEspecialidade(){
    let url = this.url + 'especialidade/listar'
    let response;
    response = this.httpService.get(url)
                  .map(res => res.json())
                  .catch(this.handleError);
      return response;
 
  }

   handleError(error: Response) {
        console.log(error);
        return Observable.throw(error.json() || 'Server error');
    }

}
