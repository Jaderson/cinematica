import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ResponseAutenticacaoDTO } from '../model/ResponseAutenticacaoDTO';
import { HttpService } from './HttpService';
import { LoadingUtil } from '../util/LoadingUtil';
import { ToastController } from 'ionic-angular';

/*
  Generated class for the LoginServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class LoginService {

  respostaErro: ResponseAutenticacaoDTO = new ResponseAutenticacaoDTO();
  url: string = 'http://www.cinematicaesportiva.com.br/fisio-web/api/auth/login/';
  urlLocal: string = 'http://localhost:8080/fisio-web/api/auth/login/';


  constructor(public httpService: HttpService, public loadingUtil: LoadingUtil, private toastCtrl: ToastController) { }

  ionViewDidLoad() { }


  public login(usuario:String, senha:String){
    this.loadingUtil.abrirLoading();
    return new Promise((resolve, reject) => {

      let body = 'username=' + usuario + '&senha=' + senha;
      try {
        this.httpService.postLogin(this.url, body, null)
          .map(res => res.json())
          .subscribe(data => {
          }, error => {
            /*this.respostaErro.mensagem =  "Login e/ou senha não coincidem. Por favor, tente novamente!"*/
            let toast = this.toastCtrl.create({
              message: 'Login e/ou senha não coincidem. Por favor, tente novamente!',
              duration: 3000,
              position: 'top'
            });

            toast.present();
            this.loadingUtil.fecharLoading();
            /*reject(this.presentToast());*/
          });
      } catch (e) {
        console.log(JSON.parse(e));
        this.respostaErro.mensagem = e;
        reject(this.respostaErro);
      }finally{
        this.loadingUtil.fecharLoading();
      }
    });
  }

}
