import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { LoadingUtil } from '../util/LoadingUtil';
import { HttpService } from "./HttpService";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

/*
  Generated class for the Horario provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class HorarioService {

  url: String = 'http://www.cinematicaesportiva.com.br/fisio-web/api/';
  urlLocal: string = 'http://localhost:8080/fisio-web/api/';
  
  constructor(public httpService: HttpService, public loadingUtil: LoadingUtil) { }

  public getHorario(idFuncionario: Number, data: String) {
    let url = this.url + 'agendas/listar/horario/' + idFuncionario + '/' + data;
    let response;
    response = this.httpService.get(url)
      .map(res => res.json())
      .catch(this.handleError);
    return response;

  }

  handleError(error: Response) {
    console.log(error);
    return Observable.throw(error.json() || 'Server error');
  }

}
