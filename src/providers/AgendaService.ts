import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from './HttpService';
import { LoadingUtil } from '../util/LoadingUtil';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

/*
  Generated class for the Agenda provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AgendaService {

   url:String = 'http://www.cinematicaesportiva.com.br/fisio-web/api/';
   urlLocal: string = 'http://localhost:8080/fisio-web/api/';

  constructor(public httpService: HttpService, public loadingUtil:LoadingUtil) {}

  public getListAgenda(idEmpresa:Number, idUsuario:Number){
    let url = this.url + 'agendas/listar/' + idEmpresa + '/' + idUsuario;
    let response;
    response = this.httpService.get(url)
                  .map(res => res.json())
                  .catch(this.handleError);
      return response;
 
  }

  public cadastrarSolicitacaoAgenda(idPessoa: Number, idEspecialidade: Number,
    idFuncionario: Number, data: String, idHorario: Number, idEmpresa: Number) {
    let url = this.url + 'agendas/cadastrar/solicitacao/' + idPessoa + '/' + idEspecialidade + '/' + idFuncionario + '/' + data + '/' + idHorario + '/' + idEmpresa;;
    let response;
    response = this.httpService.get(url)
      .map(res => res.json())
      .catch(this.handleError);
    return response;
  }

  public deleteAgenda(idAgenda:Number){
    let url = this.url + 'agendas/delete/' + idAgenda;
    let response;
    response = this.httpService.get(url)
                  .map(res => res.json())
                  .catch(this.handleError);
      return response;
 
  }
  
   handleError(error: Response) {
        console.log(error);
        return Observable.throw(error.json() || 'Server error');
    }
}
