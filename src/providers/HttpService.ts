import { Injectable } from '@angular/core';
import { Http, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
/*
  Generated class for the HttpService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class HttpService {

  private headers: Headers;

  constructor(public http: Http) { }

  get(url) {
    return this.http.get(url, { headers: this.headers });
  }

  post(url: string, body: any) {
    this.getGenerico(url, body);
  }

  private getGenerico(url: string, bodyP: any) {
    return this.http.post(url,
      bodyP ? bodyP : {},
      { headers: new Headers() })
      .map(res => res.json);
  }

  public postLogin(url: string, bodyP: any, headerP: any) {

    console.log(url);
    let header = headerP ? headerP : this.getHeaderBasico();

    return this.http.post(url,
      bodyP ? bodyP : {},
      { headers: header });
  }


  public getHeaderBasico() {
    let header: Headers = new Headers();
    header.append('Content-Type', 'application/x-www-form-urlencoded');
    return header;
  }

}
