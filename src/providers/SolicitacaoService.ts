import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpService } from "./HttpService";
import { Observable } from "rxjs/Observable";
import { LoadingUtil } from "../util/LoadingUtil";
import * as moment from 'moment';


@Injectable()
export class SolicitacaoService {

  url:String = 'http://www.cinematicaesportiva.com.br/fisio-web/api/';
  urlLocal: string = 'http://localhost:8080/fisio-web/api/';

  constructor(public httpService: HttpService,public loadingUtil:LoadingUtil) {}

  public consultarSolicitacao(idEmpresa:Number, idPessoa:Number){
    let url = this.url + 'agendas/listar/solicitacao/' + idEmpresa + '/' + idPessoa;  
    let response;
    response = this.httpService.get(url)
                  .map(res => res.json())
                  .catch(this.handleError);
    console.log(response);
      return response;
 
  }

  public deleteSolicitacao(idSolicitacao:Number){
    let url = this.url + 'agendas/delete/solicitacao/' + idSolicitacao
    let response;
    response = this.httpService.get(url)
                  .map(res => res.json())
                  .catch(this.handleError);
      return response;
  }

   handleError(error: Response) {
        console.log(error);
        return Observable.throw(error.json() || 'Server error');
    }
}
