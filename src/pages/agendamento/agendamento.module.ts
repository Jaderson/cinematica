import { NgModule } from '@angular/core';
import { IonicModule, IonicPageModule } from 'ionic-angular';
import { Agendamento } from './agendamento';

@NgModule({
  declarations: [
    Agendamento,
  ],
  imports: [
    IonicPageModule.forChild(Agendamento),
  ],
  exports: [
    Agendamento
  ]
})
export class AgendamentoModule {}
