import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, List } from 'ionic-angular';
import { FuncionarioService } from '../../providers/FuncionarioService'
import { HttpService } from "../../providers/HttpService";
import { FuncionarioDTO } from '../../model/FuncionarioDTO';
import { LoadingUtil } from '../../util/LoadingUtil';
import { HorarioService } from '../../providers/HorarioService';
import { Datas } from '../datas/datas'
import { Especialidade } from "../especialidade/especialidade";
import { AgendamentoDTO } from "../../model/AgendamentoDTO";
import { AppModule } from "../../app/app.module";
import { MyApp } from "../../app/app.component";

/**
 * Generated class for the Agendamento page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-agendamento',
  templateUrl: 'agendamento.html',
  providers:[HttpService,FuncionarioService,LoadingUtil]
})
export class Agendamento {

  listaFuncionarioDTO : Array<FuncionarioDTO> = new Array<FuncionarioDTO>();

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public funcionarioService:FuncionarioService, public loadingUtil : LoadingUtil) {
    this.exibirFuncionarios();
  }

  ionViewDidLoad() {}

  public exibirFuncionarios(){
    this.loadingUtil.abrirLoading();
    this.funcionarioService.getListFuncionario(1).subscribe(
        data => {
          for (var index = 0; index < data.results.length; index++) {
            this.listaFuncionarioDTO.push(data.results[index]);
          }
        },
        err => {
          console.log(err);
          this.loadingUtil.fecharLoading();
        },
        () => {
           this.loadingUtil.fecharLoading();
        }
      );
    }

     itemTapped(event, item) {
       this.navCtrl.push(Especialidade,item);
    }
}
