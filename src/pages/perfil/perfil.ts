import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PerfilService } from '../../providers/PerfilService';
import { HttpService } from '../../providers/HttpService';
import { Observable } from 'rxjs/Observable';
import { PerfilDTO } from '../../model/PerfilDTO';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { LoadingUtil } from "../../util/LoadingUtil";
import { AppModule } from "../../app/app.module";
import { MyApp } from "../../app/app.component";
import * as moment from 'moment';
import 'moment/locale/pt-br';
import { Login } from "../login/login";

/**
 * Generated class for the Perfil page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
  providers:[HttpService,PerfilService,LoadingUtil]
})
export class Perfil {
 
  perfilDTO = new PerfilDTO();

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public perfilService:PerfilService, public loadingUtil : LoadingUtil) {
    moment.locale('pt-br');
  }

  ionViewDidLoad() {
    this.exibirPerfil();
  }

  public exibirPerfil(){
    this.loadingUtil.abrirLoading();
    this.perfilService.visualizarPerfil(1,'jaderson').subscribe(
        data => {

          this.perfilDTO.nome = data.results[0].nome;
          this.perfilDTO.email = data.results[0].email;
          this.perfilDTO.dataNascimento = data.results[0].dataNascimento;
          this.perfilDTO.telefone = data.results[0].telefoneCelular;
          if(data.results[0].sexo == 'M'){
            this.perfilDTO.sexo = 'Masculino';
          }else{
            this.perfilDTO.sexo  = 'Feminino';
          }
        },
        err => {
          console.log(err);
          this.loadingUtil.fecharLoading();
        },
        () => {
          this.loadingUtil.fecharLoading();
        }
      );
    }

  public logout(){
    this.navCtrl.setRoot(Login);
  }
}
