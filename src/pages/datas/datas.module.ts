import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Datas } from './datas';

@NgModule({
  declarations: [
    Datas,
  ],
  imports: [
    IonicPageModule.forChild(Datas),
  ],
  exports: [
    Datas
  ]
})
export class DatasModule {}
