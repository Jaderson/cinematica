import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingUtil } from '../../util/LoadingUtil';
import { HttpService } from "../../providers/HttpService";
import { TextMaskModule } from 'angular2-text-mask';
import { HorarioDTO } from "../../model/HorarioDTO";
import { HorarioService } from "../../providers/HorarioService";
import { Horario } from "../horario/horario";
import { AgendamentoDTO } from "../../model/AgendamentoDTO";
import { CalendarController } from "ion2-calendar/dist";
import { DatePicker } from 'ionic2-date-picker';
import { DatePickerDirective } from 'datepicker-ionic2';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import { ViewChild } from 'ionic2-date-picker/node_modules/@angular/core';


/**
 * Generated class for the Datas page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-datas',
  templateUrl: 'datas.html',
  providers: [HttpService, HorarioService, LoadingUtil, DatePicker, DatePickerDirective]
})
export class Datas {

  /*@ViewChild(DatePickerDirective) public datePickerDirective: DatePickerDirective;*/
  public localDate: Date = new Date();
  public initDate: Date = new Date();
  public initDate2: Date = new Date(2015, 1, 1);
  public disabledDates: Date[] = [new Date(2017, 7, 14)];
  public maxDate: Date = new Date(new Date().setDate(new Date().getDate() + 30));
  public min: Date = new Date()

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingUtil: LoadingUtil,
    public horarioService: HorarioService, public calendarCtrl: CalendarController, public datePicker: DatePicker, public datePickerDirective: DatePickerDirective) {
    moment.locale('pt-br');
    this.apresentarDate();
  }

  ionViewDidLoad() { }

  public Log(stuff): void {
    this.datePickerDirective.open();
    this.datePickerDirective.changed.subscribe(() => console.log('test'));
    console.log(stuff);
  }

  public event(data: Date): void {
    this.localDate = data;
  }

  setDate(date: Date) {
    console.log(date);
    this.initDate = date;
  }

  /*public apresentarDate(){
    this.datePicker.showCalendar();
    this.datePicker.onDateSelected.subscribe( 
      (date) => {
        let listAgendamentoDTO = new Array<AgendamentoDTO>();
        let agendamentoDTO = new AgendamentoDTO();       
        agendamentoDTO.dateSelecionada = moment(date).format('L').toString();
        listAgendamentoDTO.push(agendamentoDTO);
        listAgendamentoDTO.push(this.navParams.data);
    
        this.navCtrl.push(Horario, listAgendamentoDTO); 
    });
  }*/

  /*apresentarDate() {
    let listAgendamentoDTO = new Array<AgendamentoDTO>();
    let agendamentoDTO = new AgendamentoDTO();
    agendamentoDTO.dateSelecionada = this.dateSelecionada;
    listAgendamentoDTO.push(agendamentoDTO);
    listAgendamentoDTO.push(this.navParams.data);

    this.navCtrl.push(Horario, listAgendamentoDTO);
  }*/

  apresentarDate() {
    this.calendarCtrl.openCalendar({
      from:new Date(),
      title:'Calendário',
      weekdaysTitle:['D','S','T','Q','Q','S','S'],
      closeLabel:'',
      monthTitle:'MM - yyyy',
      disableWeekdays:[0,6]
    })
    .then( res => { 
      console.log(res); 
      console.log(moment(new Date(res.time)).format('l'));
      let listAgendamentoDTO = new Array<AgendamentoDTO>();
      let agendamentoDTO = new AgendamentoDTO();
      let date = new Date(res.date.time);
      agendamentoDTO.dateSelecionada = moment(date).format('L').toString();
      listAgendamentoDTO.push(agendamentoDTO);
      listAgendamentoDTO.push(this.navParams.data);
  
      this.navCtrl.push(Horario, listAgendamentoDTO); 
    } );
  }
}