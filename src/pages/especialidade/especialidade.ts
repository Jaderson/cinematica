import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EspecialidadeDTO } from "../../model/EspecialidadeDTO";
import { HttpService } from "../../providers/HttpService";
import { EspecialidadeService } from "../../providers/EspecialidadeService";
import { LoadingUtil } from "../../util/LoadingUtil";
import { Datas } from "../datas/datas";
import { AgendamentoDTO } from "../../model/AgendamentoDTO";

/**
 * Generated class for the Especialidade page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-especialidade',
  templateUrl: 'especialidade.html',
  providers:[HttpService,EspecialidadeService,LoadingUtil]
})
export class Especialidade {

  listaEspecialidadeDTO: Array<EspecialidadeDTO> = new Array<EspecialidadeDTO>();

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public especialidadeService:EspecialidadeService, public loadingUtil : LoadingUtil) {
    this.exibirEspecialidade();
  }
  ionViewDidLoad() {}

 public exibirEspecialidade(){
    this.loadingUtil.abrirLoading();
    this.especialidadeService.getListEspecialidade().subscribe(
        data => {
          for (var index = 0; index < data.results.length; index++) {
            this.listaEspecialidadeDTO.push(data.results[index]);
          }
        },
        err => {
          console.log(err);
          this.loadingUtil.fecharLoading();
        },
        () => {
           this.loadingUtil.fecharLoading();
        }
      );
    }

    itemTapped(event, item) {
      let listAgendamentoDTO = new Array<AgendamentoDTO>();
      listAgendamentoDTO.push(this.navParams.data);
      listAgendamentoDTO.push(item);
      
      this.navCtrl.push(Datas,listAgendamentoDTO);
    }
}
