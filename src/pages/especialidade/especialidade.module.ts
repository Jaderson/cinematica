import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Especialidade } from './especialidade';

@NgModule({
  declarations: [
    Especialidade,
  ],
  imports: [
    IonicPageModule.forChild(Especialidade),
  ],
  exports: [
    Especialidade
  ]
})
export class EspecialidadeModule {}
