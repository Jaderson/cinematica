import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, IonicApp, MenuController, ToastController } from 'ionic-angular';
import { HttpService } from "../../providers/HttpService";
import { LoadingUtil } from "../../util/LoadingUtil";
import { HomePage } from "../home/home";
import { MyApp } from "../../app/app.component";
import { ResponseAutenticacaoDTO } from "../../model/ResponseAutenticacaoDTO";

/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [HttpService, LoadingUtil]
})
export class Login {

  menu: MenuController;

  @Input() usuario: String;
  @Input() senha: String;
  respostaErro: ResponseAutenticacaoDTO = new ResponseAutenticacaoDTO();
  url: string = 'http://www.cinematicaesportiva.com.br/fisio-web/api/auth/login/';
  urlLocal: string = 'http://localhost:8080/fisio-web/api/auth/login/';


  constructor(public navCtrl: NavController, public navParams: NavParams, menu: MenuController,
    public httpService: HttpService, private toastCtrl: ToastController) {
    this.menu = menu;
    menu.enable(false);
  }

  ionViewDidLoad() { }

  public login() {

    return new Promise((resolve, reject) => {

      let body = 'username=' + this.usuario + '&senha=' + this.senha;
      try {

        this.httpService.postLogin(this.url, body, null)
          .map(res => res.json())
          .subscribe(data => {
            this.navCtrl.setRoot(HomePage);
            this.menu.enable(true);
          }, error => {
            /*this.respostaErro.mensagem =  "Login e/ou senha não coincidem. Por favor, tente novamente!"*/
            let toast = this.toastCtrl.create({
              message: 'Login e/ou senha não coincidem. Por favor, tente novamente!',
              duration: 3000,
              position: 'top'
            });

            toast.present();
            /*reject(this.presentToast());*/
          });
      } catch (e) {
        console.log(JSON.parse(e));
        this.respostaErro.mensagem = e;
        reject(this.respostaErro);
      }
    });
  }
}
