import { Component, Input } from '@angular/core';
import { NavController, NavParams, ActionSheetController, Refresher } from 'ionic-angular';
import { AgendaService } from "../../providers/AgendaService";
import { HttpService } from "../../providers/HttpService";
import { LoadingUtil } from "../../util/LoadingUtil";
import { AgendaDTO } from "../../model/AgendaDTO";
import { MyApp } from "../../app/app.component";
import * as moment from 'moment';
import 'moment/locale/pt-br';
import { AppModule } from "../../app/app.module";
import { AnonymousSubscription } from "rxjs/Subscription";
import { Observable } from "rxjs/Rx";
import { Agendamento } from "../agendamento/agendamento";
import { SolicitacaoPage } from '../solicitacao/solicitacao';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [HttpService, AgendaService, LoadingUtil]
})
export class HomePage {
  
  listaAgendaDTO: Array<AgendaDTO> = new Array<AgendaDTO>();
  private timerSubscription: AnonymousSubscription;
  tab1Root = Agendamento;
  tab2Root = SolicitacaoPage;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public agendaService: AgendaService, public loadingUtil: LoadingUtil,
    public actionSheetCtrl: ActionSheetController) {
  }

  ionViewDidLoad() { 
    this.exibirAgendamentos();
  }

  public doRefresh(refresher) {
    this.atualizarLista();
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  public abrirAgendamento(){
    this.navCtrl.setRoot(Agendamento);
  }

  public doInfinite(infiniteScroll) {

    setTimeout(() => {
      for (let item = 0; item < 30; item++) {
        this.atualizarLista();
      }

      console.log('Infinite');
      infiniteScroll.complete();
    }, 500);
  }

  public atualizarLista(){
    this.listaAgendaDTO = new Array<AgendaDTO>();
    this.exibirAgendamentos();
  }

  public exibirAgendamentos() {
    this.loadingUtil.abrirLoading();
    this.agendaService.getListAgenda(1, 1).subscribe(
      data => {
        for (var index = 0; index < data.results.length; index++) {

          let id = data.results[index].id;
          let funcionario = data.results[index].funcionario.nome;
          let especialidade = data.results[index].especialidade.descricao;
          let horario = data.results[index].horario.horarioInicio + '-' + data.results[index].horario.horarioFim;
          let date = moment(data.results[index].dataInicio).format('l');
          console.log(date);

          let dto = new AgendaDTO();
          dto.id = id;
          dto.funcionario = funcionario;
          dto.especialidade = especialidade;
          dto.horario = horario;
          dto.data = date;

          this.listaAgendaDTO.push(dto);
        }
      },
      err => {
        console.log(err);
        this.loadingUtil.fecharLoading();
      },
      () => {
        this.loadingUtil.fecharLoading();
      }
    );
  }


  private subscribeDados(): void {
    this.timerSubscription = Observable.timer(1000).first().subscribe(() => this.atualizarLista());
  }

  public deleteAgenda(item){
   this.presentActionSheet(item);
  }

  public presentActionSheet(item) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Deseja realmente cancelar o agendamento?',
      buttons: [
        {
          text: 'Sim',
          role: 'destructive',
          handler: () => {
            this.agendaService.deleteAgenda(item.id).subscribe(
              data => {
                this.subscribeDados();
              },
              err => {
                console.log(err);
                this.loadingUtil.fecharLoading();
              },
              () => {
                this.subscribeDados();
                this.loadingUtil.fecharLoading();
              }
            );
          }
        }, {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            console.log('Cancelado');
          }
        }
      ]
    });
    actionSheet.present();
  }
}
