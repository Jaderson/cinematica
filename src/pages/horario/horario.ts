import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HorarioService } from "../../providers/HorarioService";
import { LoadingUtil } from "../../util/LoadingUtil";
import { HorarioDTO } from "../../model/HorarioDTO";
import { HttpService } from "../../providers/HttpService";
import { AlertController } from 'ionic-angular';
import { Agendamento } from "../agendamento/agendamento";
import { HomePage } from "../home/home";
import { PerfilDTO } from "../../model/PerfilDTO";
import { MyApp } from "../../app/app.component";
import * as moment from 'moment';
import 'moment/locale/pt-br';
import { AgendaDTO } from "../../model/AgendaDTO";
import { AgendaService } from "../../providers/AgendaService";

/**
 * Generated class for the Horario page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-horario',
  templateUrl: 'horario.html',
  providers: [HttpService, HorarioService, LoadingUtil, AgendaService]
})
export class Horario {

  listaHorarioDTO: Array<HorarioDTO> = new Array<HorarioDTO>();
  nomeFuncionario: String;
  nomeEspecialidade: String;
  listaAgendaDTO: Array<AgendaDTO> = new Array<AgendaDTO>();

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingUtil: LoadingUtil,
    public horarioService: HorarioService, public agendaService: AgendaService, public alerCtrl: AlertController) {

    this.apresentarHorarioDisponivel();
    moment.locale('pt-br');
  }

  ionViewDidLoad() { }

  apresentarHorarioDisponivel() {

    let funcionario = this.navParams.data[1];
    let data = this.navParams.data[0].dateSelecionada.replace('/', '').replace('/', '');
    this.nomeFuncionario = funcionario[0].nome;
    this.nomeEspecialidade = funcionario[1].descricao;

    this.loadingUtil.abrirLoading();
    this.horarioService.getHorario(funcionario[0].id, data).subscribe(
      data => {
        for (var index = 0; index < data.results.length; index++) {
          this.listaHorarioDTO.push(data.results[index]);
        }
      },
      err => {
        console.log(err);
        this.loadingUtil.fecharLoading();
      },
      () => {
        this.loadingUtil.fecharLoading();
      }
    );
  }

  showConfirm(item) {
    let funcionario = this.navParams.data[1];
    let data = moment(this.navParams.data[0].dateSelecionada).format('l');
    this.nomeFuncionario = funcionario[0].nome;
    this.nomeEspecialidade = funcionario[1].descricao;

    let idPessoa = 1;
    let idEspecialidade = funcionario[1].id;
    let idFuncionario = funcionario[0].id;
    let idEmpresa = funcionario[0].empresa.id;
    let idHorario = item.id;
    let dateSelecionada = moment(this.navParams.data[0].dateSelecionada).format('L');
    console.log(dateSelecionada.replace('/', '').replace('/', ''));

    let confirm = this.alerCtrl.create({
      title: 'Agendamento?',
      message: 'Funcionário: ' + this.nomeFuncionario + '<br />Especialidade: ' + this.nomeEspecialidade
      + '<br />Data: ' + data + '<br />Horário: ' + item.horarioInicio + ' - ' + item.horarioFim,
      buttons: [
        {
          text: 'Confirmar',
          handler: () => {
            this.agendaService.cadastrarSolicitacaoAgenda(idPessoa, idEspecialidade, idFuncionario, 
              dateSelecionada.replace('/', '').replace('/', ''), idHorario, idEmpresa)
              .subscribe(
                data => {console.log(data);},
                err => {
                  console.log(err);
                  this.loadingUtil.fecharLoading();
                },
                () => {
                  this.loadingUtil.fecharLoading();
                }
              );
            this.navCtrl.setRoot(HomePage);
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
            /*this.navCtrl.push(MyApp);*/
          }
        }
      ]
    });
    confirm.present();
  }
}
