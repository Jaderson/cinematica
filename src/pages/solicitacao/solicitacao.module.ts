import { NgModule } from '@angular/core';
import { IonicModule, IonicPageModule } from 'ionic-angular';
import { SolicitacaoPage } from './solicitacao';

@NgModule({
  declarations: [
    SolicitacaoPage,
  ],
  imports: [
    IonicPageModule.forChild(SolicitacaoPage),
  ],
  exports: [
    SolicitacaoPage
  ]
})
export class SolicitacaoPageModule {}
