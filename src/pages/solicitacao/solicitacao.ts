import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController } from 'ionic-angular';
import { SolicitacaoService } from "../../providers/SolicitacaoService";
import { SolicitacaoDTO } from "../../model/SolicitacaoDTO";
import { LoadingUtil } from "../../util/LoadingUtil";
import { Agendamento } from "../agendamento/agendamento";
import { AnonymousSubscription } from "rxjs/Subscription";
import { Observable } from "rxjs/Rx";
import * as moment from 'moment';

/**
 * Generated class for the SolicitacaoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-solicitacao',
  templateUrl: 'solicitacao.html',
})
export class SolicitacaoPage {

  listaSolicitacaoDTO: Array<SolicitacaoDTO> = new Array<SolicitacaoDTO>();
  private timerSubscription: AnonymousSubscription;

  constructor(public navCtrl: NavController, public navParams: NavParams,
     public solicitacaoService:SolicitacaoService, public loadingUtil:LoadingUtil,
     public actionSheetCtrl: ActionSheetController) {
       this.apresentarSolicitacaoPendente();
  }

  ionViewDidLoad() {
  }

  public doRefresh(refresher) {
    this.atualizarLista();
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  public apresentarSolicitacaoPendente(){
   
    this.loadingUtil.abrirLoading();
    this.solicitacaoService.consultarSolicitacao(1,1).subscribe(
      data => {
        console.log(data);
        for (var index = 0; index < data.results.length; index++) {
          let id = data.results[index].id;
          let funcionario = data.results[index].funcionario.nome;
          let especialidade = data.results[index].especialidade.descricao;
          let horario = data.results[index].horario.horarioInicio + '-' + data.results[index].horario.horarioFim;
          let date = moment(data.results[index].dataInicio).format('l');
          console.log(date);

          let dto = new SolicitacaoDTO();
          dto.id = id;
          dto.funcionario = funcionario;
          dto.especialidade = especialidade;
          dto.horario = horario;
          dto.data = date;

          this.listaSolicitacaoDTO.push(dto);
        }
      },
      err => {
        console.log(err);
        this.loadingUtil.fecharLoading();
      },
      () => {
        this.loadingUtil.fecharLoading();
      }
    );
  }

  public abrirAgendamento(){
    this.navCtrl.setRoot(Agendamento);
  }

  public atualizarLista(){
    this.listaSolicitacaoDTO = new Array<SolicitacaoDTO>();
    this.apresentarSolicitacaoPendente();
  }


  private subscribeDados(): void {
    this.timerSubscription = Observable.timer(1000).first().subscribe(() => this.atualizarLista());
  }

  public deleteSolicitacao(item){
    this.presentActionSheet(item);
  }

  public presentActionSheet(item) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Deseja cancelar a solicitação de agendamento?',
      buttons: [
        {
          text: 'Sim',
          role: 'destructive',
          handler: () => {
            this.solicitacaoService.deleteSolicitacao(item.id).subscribe(
              data => {
                this.subscribeDados();
              },
              err => {
                console.log(err);
                this.loadingUtil.fecharLoading();
              },
              () => {
                this.subscribeDados();
                this.loadingUtil.fecharLoading();
              }
            );
          }
        }, {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            console.log('Cancelado');
          }
        }
      ]
    });
    actionSheet.present();
  }
}

